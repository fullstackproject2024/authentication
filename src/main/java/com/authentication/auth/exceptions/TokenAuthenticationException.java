package com.authentication.auth.exceptions;

import org.springframework.security.core.AuthenticationException;

public class TokenAuthenticationException extends AuthenticationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TokenAuthenticationException(String msg) {
		super(msg);
	}

}
