package com.authentication.auth.config.security;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.authentication.auth.daos.UserDAO;
import com.authentication.auth.exceptions.InvalidTokenException;
import com.authentication.auth.exceptions.TokenAuthenticationException;
import com.authentication.auth.models.UserModel;
import com.authentication.auth.services.TokenService;
@Component
public class AuthProvider extends AbstractUserDetailsAuthenticationProvider{

	@Autowired
	UserDAO userDAO;
	@Autowired
	TokenService tokenService;
	
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		final String token = (String) authentication.getCredentials();
		if(StringUtils.isEmpty(token)) {
			// if token does not exist, we login a user with anonymous role
			return new  User(username,"",AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS"));
		}
		
		
		
		//find user by token when token exists
		Optional<UserModel> existingUser = userDAO.findByJwtToken(token);
		if(existingUser.isPresent()) {
			final UserModel userModel = existingUser.get();
			
			try {
				tokenService.validateToken(token);
			} catch (InvalidTokenException e) {
				userModel.setJwtToken(null);
				userDAO.save(userModel);
				return null;
			}
			
			return new User(username,"",
					AuthorityUtils.createAuthorityList(
							userModel.getRoles()
							.stream()
							.map(roleName -> "ROLE_"+roleName)
							.toArray(String[]::new )));
		}
		
		throw new TokenAuthenticationException("User not found for token: "+token);
	}

}
