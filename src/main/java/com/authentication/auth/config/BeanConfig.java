package com.authentication.auth.config;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.authentication.auth.dtos.UserInfoDTO;
import com.authentication.auth.models.UserModel;

@Configuration
public class BeanConfig {

	
	// singleton object will be created by spring context
	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper =  new ModelMapper();
		
		// add a new typeMap between UserModel and UserInfoDTO
		//skip setPassword() of destination when transforming from UserModel to UserModelDTO
		final TypeMap<UserModel, UserInfoDTO> userModelUserInfoDTOTypeMap=
		modelMapper.typeMap(UserModel.class,UserInfoDTO.class);
		userModelUserInfoDTOTypeMap.addMappings(
				mapping-> mapping.skip(UserInfoDTO::setPassword));
		
		//stops overwriting values on destination, when source value is null
		modelMapper.getConfiguration().setSkipNullEnabled(true);		
		return modelMapper;
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
//	@Bean
//	public CorsFilter corsFilter() {
//		
//		UrlBasedCorsConfigurationSource configSource = new UrlBasedCorsConfigurationSource();
//		
//		CorsConfiguration config = new CorsConfiguration();
//		//GET,POST,HEAD
//		config.applyPermitDefaultValues();
//		config.addAllowedMethod(HttpMethod.PUT);
//		config.addAllowedMethod(HttpMethod.DELETE);
//		config.addAllowedMethod(HttpMethod.OPTIONS);
//		
//		configSource.registerCorsConfiguration("/**", config);
//		
//		return new CorsFilter(configSource);
//	}
	
}
