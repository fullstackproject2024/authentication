package com.authentication.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;

import com.authentication.auth.config.security.AuthFilter;
import com.authentication.auth.config.security.AuthProvider;
@Configuration
@EnableWebSecurity

public class WebSecurityConfig {
	
	@Autowired
	private AuthProvider authProvider;
	

	@Autowired
	private AuthenticationConfiguration authenticationConfiguration;

	 @Bean
	    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
	        http

//	            .formLogin(form -> form
//	                    .loginPage("/login") // Specify the custom login page
//	                    .permitAll() // Allow access to the login page
//	            )
//	            .logout(logout -> logout
//	                    .permitAll()
	        
	        .sessionManagement(management -> management
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS))
	        .authenticationProvider(authProvider)
	        .addFilterBefore(authFilter(),AnonymousAuthenticationFilter.class)
	        .authorizeHttpRequests(auth -> auth
	        		.requestMatchers("/**","/swagger-ui/**").permitAll()
	        		.anyRequest().authenticated())
	        .csrf(csrf -> csrf.disable())// we don't use cookies to auth users
	        .httpBasic(httpBasic -> httpBasic.disable())
	        .logout(logout -> logout.disable())
	        .cors(AbstractHttpConfigurer::disable);
	        return http.build();
	    }
	
	 public AuthFilter authFilter() throws Exception {
		 //determine the paths this filter should apply to
		 OrRequestMatcher orRequstMatcher = new OrRequestMatcher(
				 new AntPathRequestMatcher("/user/**"),
				 new AntPathRequestMatcher("/token/**"),
				 new AntPathRequestMatcher("/role/**")
				 );
		 AuthFilter authFilter = new AuthFilter(orRequstMatcher);
		 authFilter.setAuthenticationManager(authenticationConfiguration.getAuthenticationManager());
		 return authFilter;
		 
	 }
	
}
