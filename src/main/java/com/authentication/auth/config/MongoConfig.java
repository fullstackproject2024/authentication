package com.authentication.auth.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

@Configuration
public class MongoConfig extends AbstractMongoClientConfiguration{

	@Value("${spring.data.mongodb.host}")
	private String host;
	
	@Value("${spring.data.mongodb.database}")
	private String databse;
	
	@Override
	protected String getDatabaseName() {
		return this.databse;
	}

	@Override
	public MongoClient mongoClient() {
		return MongoClients.create(this.host);
	}

	@Override
	protected boolean autoIndexCreation() {
		return true;
	}

}
