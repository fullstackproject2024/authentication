package com.authentication.auth.models;

import java.util.Collection;
import java.util.List;

import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Document(collection="users")
@EqualsAndHashCode(callSuper=true)// is needed by Lombok as is extended the GenericModel class and not the Object class
public class UserModel extends GenericModel implements UserDetails{
	
	
	//@Indexed => marks the unique fields (mongo write exception will be raised)
	@Indexed(direction= IndexDirection.DESCENDING, unique=true)
	private String username;
	//@Indexed => marks the unique fields (mongo write exception will be raised)
	@Indexed(direction= IndexDirection.DESCENDING, unique=true)
	private String displayName;	
	//@Indexed => marks the unique fields (mongo write exception will be raised)
	@Indexed(direction= IndexDirection.DESCENDING, unique=true)
	private String email;	
	private List<String> roles;
	private String jwtToken;	
	private String password;
	
	//the constructor calls super() constructor which is the constructor of the super class
	//so it will have an ID generator from the super class (GenericModel)
	public UserModel() {
		super();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		//return (Collection<? extends GrantedAuthority>) List.of(roles);
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
}
