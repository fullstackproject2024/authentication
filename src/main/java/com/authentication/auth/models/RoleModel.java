package com.authentication.auth.models;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Document(collection="roles")
@EqualsAndHashCode(callSuper=true)// is needed by Lombok as is extended the GenericModel class and not the Object class
public class RoleModel extends GenericModel {
	
	private String role;
	
	//the constructor calls super() constructor which is the constructor of the super class
	//so it will have an ID generator from the super class (GenericModel)
	public RoleModel() {
		super();
	}
}
