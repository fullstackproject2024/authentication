package com.authentication.auth.dtos;

import lombok.Data;

@Data
public class UserLoginDTO {

	private String username;
	private String password;
}
