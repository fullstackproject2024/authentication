package com.authentication.auth.services.impl;

import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.authentication.auth.daos.RoleDAO;
import com.authentication.auth.daos.UserDAO;
import com.authentication.auth.dtos.RoleDTO;
import com.authentication.auth.dtos.UserInfoDTO;
import com.authentication.auth.dtos.UserLoginDTO;
import com.authentication.auth.models.RoleModel;
import com.authentication.auth.models.UserModel;
import com.authentication.auth.services.TokenService;
import com.authentication.auth.services.UserService;
import com.google.common.base.Preconditions;

@Service
public class UserServiceImpl implements UserService {

	private static String UNKNOWN_USERNAME_OR_BAD_PASSWORD = "Unknown username or bad password";
	@Autowired
	ModelMapper modelMapper; // this property needs to be defined as a bean (BeanConfig)	
	@Autowired
	UserDAO userDAO;	
	@Autowired
	RoleDAO roleDAO;	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder; //this property is not a bean and needs to be defined as a bean (BeanConfig)	
	@Autowired
	TokenService tokenService;
	
	
	@Override
	public void createUser(UserInfoDTO userInfoDTO) {
		
		Preconditions.checkNotNull(userInfoDTO.getPassword());		
		
		// convert UserInfoDTO to UserModel
		UserModel userModel= modelMapper.map(userInfoDTO, UserModel.class);
		
		
		//hash the password when firstly creating the user
		userModel.setPassword(bCryptPasswordEncoder.encode(userInfoDTO.getPassword()));
		
		
		//assign default role
		userModel.setRoles(
			roleDAO.findAll().stream()
				.map(RoleModel::getRole)
				.filter(role -> role.contains("USER"))
				.collect(Collectors.toList()));
		
		//generate a new token for the user just created
		tokenService.generateToken(userModel);
		
		//save user model into the DB
		userDAO.save(userModel);
		
		//set password to empty so it cant be red on the response
		userInfoDTO.setPassword("");		
		
		// update the userInfoDTO after the userModel has been saved
		// return back the data saved into the DB as a UserInfoDTO object
		modelMapper.map(userModel, userInfoDTO);
	}

	@Override
	public UserInfoDTO retrieveUserInfo(String userId) {
		
		 Optional<UserModel> optionalUserModel = userDAO.findById(userId);
		 if(optionalUserModel.isPresent()) {
			 return modelMapper.map(optionalUserModel.get(),UserInfoDTO.class);
		 }
		 return null;
	}

	@Override
	public UserInfoDTO loginUser(UserLoginDTO userLoginDTO) {

		Optional<UserModel> optionalUserModel = userDAO.findByUsername(userLoginDTO.getUsername());


		if(optionalUserModel.isPresent()){
			UserModel userModel = optionalUserModel.get();
			//check if password matches
			if(bCryptPasswordEncoder.matches(userLoginDTO.getPassword(), userModel.getPassword())) {
				return modelMapper.map(userModel, UserInfoDTO.class);
			}else {
				throw new BadCredentialsException(UNKNOWN_USERNAME_OR_BAD_PASSWORD);
			}
		}else {
			throw new BadCredentialsException(UNKNOWN_USERNAME_OR_BAD_PASSWORD);
		}
	}

	@Override
	public UserInfoDTO updateUser(UserInfoDTO userToUpdate) {
		UserModel userModel = modelMapper.map(userToUpdate, UserModel.class);		
		return modelMapper.map(userDAO.save(userModel),UserInfoDTO.class);
	}

}
