package com.authentication.auth.services.impl;

import java.security.Key;
import java.util.Base64;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.authentication.auth.services.AuthSigningKeyResolver;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import static java.util.Objects.isNull;

@Component
public class AuthSigningKeyResolverImpl implements AuthSigningKeyResolver{

	//define a secret foe a key (not a good practice to store it here at the application.properties file)
	@Value("${jwt.secret.key}")
	String secretKeyString;
	
	private SecretKey secretKey;
	
	@Override
	public SecretKey getSecretKey() {
		if(isNull(secretKey)) {
			this.secretKey=Keys.hmacShaKeyFor(Base64.getEncoder()
					.encode(this.secretKeyString.getBytes()));			
			//secretKey = Keys.secretKeyFor(SignatureAlgorithm.HS512);
		}
		return this.secretKey;
	}
	
	@Override
	public Key resolveSigningKey(JwsHeader header, Claims claims) {		
		return getSecretKey();
	}

	@Override
	public Key resolveSigningKey(JwsHeader header, String plaintext) {		
		return getSecretKey();
	}



}
