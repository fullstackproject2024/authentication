package com.authentication.auth.services.impl;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.authentication.auth.daos.RoleDAO;
import com.authentication.auth.dtos.RoleDTO;
import com.authentication.auth.models.RoleModel;
import com.authentication.auth.services.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleDAO roleDAO;
	@Autowired
	ModelMapper modelMapper;
	
	@Override
	public void createRole(RoleDTO roleDTO) {
		// create model from the DTO
		RoleModel roleModel = modelMapper.map(roleDTO, RoleModel.class);		
		roleDAO.save(roleModel);		
		modelMapper.map(roleModel, roleDTO);
	}

	@Override
	public RoleDTO roleInfo(String roleId) {
		
		Optional<RoleModel> roleModelOptional = roleDAO.findById(roleId);
		if(roleModelOptional.isPresent()) {
			final RoleModel roleModel = roleModelOptional.get();
			return modelMapper.map(roleModel, RoleDTO.class);
		}
		return null;
	}

	@Override
	public void deleteRole(String roleId) {
		roleDAO.deleteById(roleId);		
	}

	@Override
	public RoleDTO updateRole(RoleDTO roleDTO) {
		// create model from the DTO
		RoleModel roleModel = modelMapper.map(roleDTO, RoleModel.class);		
		return modelMapper.map(roleDAO.save(roleModel),RoleDTO.class);
	}

}
