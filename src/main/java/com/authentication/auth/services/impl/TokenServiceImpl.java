package com.authentication.auth.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;
import org.springframework.stereotype.Service;

import com.authentication.auth.exceptions.InvalidTokenException;
import com.authentication.auth.models.UserModel;
import com.authentication.auth.services.AuthSigningKeyResolver;
import com.authentication.auth.services.TokenService;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.SignatureException;

@Service
public class TokenServiceImpl implements TokenService {

	@Autowired
	AuthSigningKeyResolver authSigningKeyResolver;
	
	@Override
	public void validateToken(String jwtToken) throws InvalidTokenException {

		try {
			Jwts.parserBuilder()
				.setSigningKeyResolver(authSigningKeyResolver)
				.build()
				.parse(jwtToken);
			
		}catch(ExpiredJwtException| MalformedJwtException| SignatureException| IllegalArgumentException e){
			throw new InvalidTokenException("Invalid token", e);
		}
		
	}

	@Override
	public void generateToken(UserModel userModel) {

		String jwtToken;

		jwtToken= Jwts.builder()
				.setSubject(userModel.getUsername())
				.setAudience(userModel.getRoles().toString())
				.setIssuer(userModel.getId())
				.signWith(authSigningKeyResolver.getSecretKey(),SignatureAlgorithm.HS512) //we have to create a key-resolver object 
				.compact(); //build token with compact method


		userModel.setJwtToken(jwtToken);
	}

}
