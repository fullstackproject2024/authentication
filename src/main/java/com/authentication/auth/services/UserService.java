package com.authentication.auth.services;

import com.authentication.auth.dtos.UserInfoDTO;
import com.authentication.auth.dtos.UserLoginDTO;

public interface UserService {

	void createUser(UserInfoDTO userInfoDTO);
	UserInfoDTO retrieveUserInfo(String userId);
	UserInfoDTO loginUser(UserLoginDTO userLogin);
	UserInfoDTO updateUser(UserInfoDTO userToUpdate);
}
