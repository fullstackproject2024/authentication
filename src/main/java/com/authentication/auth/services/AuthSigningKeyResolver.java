package com.authentication.auth.services;

import javax.crypto.SecretKey;

import io.jsonwebtoken.SigningKeyResolver;

public interface AuthSigningKeyResolver extends SigningKeyResolver {

	SecretKey getSecretKey();
}
