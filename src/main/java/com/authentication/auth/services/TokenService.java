package com.authentication.auth.services;

import com.authentication.auth.exceptions.InvalidTokenException;
import com.authentication.auth.models.UserModel;

public interface TokenService {

	void validateToken(String token) throws InvalidTokenException;

	void generateToken(UserModel userModel);
}
