package com.authentication.auth.services;

import com.authentication.auth.dtos.RoleDTO;
import com.authentication.auth.models.RoleModel;

public interface RoleService {

	void createRole (RoleDTO roleDTO);
	RoleDTO roleInfo(String roleId);
	void deleteRole(String roleId);
	RoleDTO updateRole(RoleDTO roleDTO);
}
