package com.authentication.auth.controllers;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.authentication.auth.services.TokenService;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping("/token")
@Slf4j
@CrossOrigin
public class TokenController {

	@Autowired
	private TokenService tokenService;

	//validation token endpoint is suposed to take the token from authorization
	//header on the request (this is how it cames from the front)

	//this endpoint will be called only by API getway, to check if request is legit or not
	@GetMapping("/validate")
	@PreAuthorize("hasAnyRole('ANONYMOUS','USER','ADMIN')")
	public void validateToken(HttpServletRequest httpServletRequest) throws Exception{

		//check if header exists
		String authHeader = httpServletRequest.getHeader(AUTHORIZATION);
		String token=null;
		if(!StringUtils.isEmpty(authHeader)) {
			token =authHeader.split("\\s")[1];
		}
		tokenService.validateToken(token);
	}
}
