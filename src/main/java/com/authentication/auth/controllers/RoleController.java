package com.authentication.auth.controllers;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.authentication.auth.dtos.RoleDTO;
import com.authentication.auth.services.RoleService;
import com.google.common.base.Preconditions;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/role")
@PreAuthorize("hasAnyRole('ADMIN')")
@Slf4j
@CrossOrigin
public class RoleController {

	@Autowired
	private RoleService roleService;
	@Autowired
	ModelMapper modelMapper;
	
	//Front-End & Microservices exchange data through DTOs
	
	@PostMapping("/create")
	public RoleDTO createRole(@RequestBody RoleDTO roleDTO) {
		
		Preconditions.checkNotNull(roleDTO);
		roleService.createRole(roleDTO);
		return roleDTO;
	}
	
	@GetMapping("/info/{roleId}")
	public RoleDTO getRoleInfo(@PathVariable String roleId) {
		
		return roleService.roleInfo(roleId);
	}
	
	//delete a role
	@DeleteMapping("/delete/{roleId}")
	public void deleteRole(@PathVariable String roleId) {
		roleService.deleteRole(roleId);
	}
	
	//modify a role
	@PutMapping("/update/{role}")
	public RoleDTO updateRole(@RequestBody RoleDTO role, @PathVariable String roleId) {
		RoleDTO roleToUpdate= roleService.roleInfo(roleId);
		roleToUpdate.setRole(role.getRole());
		return roleService.updateRole(roleToUpdate);
	}
}
