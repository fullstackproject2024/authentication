package com.authentication.auth.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.authentication.auth.dtos.UserInfoDTO;
import com.authentication.auth.dtos.UserLoginDTO;
import com.authentication.auth.services.UserService;
import com.google.common.base.Preconditions;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user")
@Slf4j
@CrossOrigin
public class UserController { 
	
	@Autowired
	private UserService userService;
	
	//Front-End & Microservices exchange data through DTOs
	
	@PostMapping("/create")
	@PreAuthorize("hasAnyRole('ANONYMOUS','ADMIN')")
	public UserInfoDTO createUser(@RequestBody UserInfoDTO userInfoDTO) {		
		Preconditions.checkNotNull(userInfoDTO);		
		  userService.createUser(userInfoDTO);
		  return userInfoDTO;
	}
	
	@GetMapping("/info/{userId}")
	@PreAuthorize("hasAnyRole('USER','ADMIN')")
	public UserInfoDTO getUserInfo(@PathVariable String userId) {		
		return userService.retrieveUserInfo(userId);
	}
	
	//deleteUser
	//TO DO
	
	//modifyUser
	//TO DO
	@PutMapping("/update/{userId}")
	@PreAuthorize("hasAnyRole('USER','ADMIN')")
	public UserInfoDTO updateRole(@RequestBody UserInfoDTO user, @PathVariable String userId) {
		UserInfoDTO userToUpdate= userService.retrieveUserInfo(userId);
		userToUpdate.setDisplayName(user.getDisplayName());
		userToUpdate.setEmail(user.getEmail());
		userToUpdate.setPassword(user.getPassword());
		userToUpdate.setRoles(user.getRoles());
		userToUpdate.setUsername(user.getUsername());
		return userService.updateUser(userToUpdate);
	}
	
	
	
	
	//Api to authenticate the user
	@PostMapping("/login")
	@PreAuthorize("hasAnyRole('ANONYMOUS')")
	public UserInfoDTO loginUser(@RequestBody UserLoginDTO userLoginDTO) {		
		Preconditions.checkNotNull(userLoginDTO);		
		return userService.loginUser(userLoginDTO);
	}

}
