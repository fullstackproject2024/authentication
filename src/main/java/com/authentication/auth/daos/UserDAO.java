package com.authentication.auth.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.authentication.auth.models.UserModel;

@Repository
public interface UserDAO extends MongoRepository<UserModel, String> {
	
	Optional<UserModel> findByUsername(String username);
	Optional<UserModel> findByJwtToken(String jwtToken);
	List<UserModel> findByDisplayNameOrUsernameOrEmail(String username);

}
