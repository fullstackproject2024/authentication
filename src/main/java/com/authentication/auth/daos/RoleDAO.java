package com.authentication.auth.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.authentication.auth.models.RoleModel;

@Repository
public interface RoleDAO extends MongoRepository<RoleModel, String> { 
	
	Optional<RoleModel> findById(String id);

}
